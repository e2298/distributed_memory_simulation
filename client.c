#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<pthread.h>
#include<math.h>
#include<search.h>
#include"defs.h"

unsigned short port; 
unsigned exp_mean; /*exponential mean, msec */
unsigned read_prob;
unsigned page_n; /*number of pages */
int server_socket; /* socket connected to server */
pthread_mutex_t server_lock;
pthread_mutex_t rand_lock; /* lock on rand() function */
pthread_mutex_t print_lock; 

int* pages; /* version of pages, -1 when out-of-date */
pthread_mutex_t* page_locks; /*lock on pages and all other related things */
int* own; /* 1 when owner of page, 0 when not */
int* available_pages; /* pages that can be requested */
int available_pages_n;
pthread_attr_t threads_attr; /* attributes for all threads */

void** readers_list; /*trees full of struct in_addr*/
typedef struct {struct in_addr addr; int socketfd; pthread_mutex_t lock;} addr_sock_lock;
typedef struct {struct in_addr addr; int pageno;} addr_page; /*holds an addr and a page number */
void* tree = NULL; /* tree full of addr_sock_lock */
pthread_mutex_t tree_lock;
/*compares ints pointed to by a and b, 
 * also structs whose first members are ints or other 32-bit entities */
int cmp_node(const void* a, const void* b){return *(int*)a - *(int*)b;}

addr_sock_lock* get_socket(struct in_addr ip){
	pthread_mutex_lock(&tree_lock);	
	addr_sock_lock* key = calloc(1, sizeof(addr_sock_lock));
	addr_sock_lock* res;
	key -> addr = ip;
	res = tsearch(key, &tree, cmp_node);
	res = *(void**) res;

	if(res == key){
		pthread_mutex_init(&(res -> lock), NULL);
		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_port = port;
		addr.sin_addr = ip;
		memset(&(addr.sin_zero), 0, 8);
		res -> socketfd = socket(PF_INET, SOCK_STREAM, 0);
		connect(res -> socketfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	}
	else free(key);

	pthread_mutex_unlock(&tree_lock);	
	return res;
}

/* only call from twalk */
void notify_single(const void* node, const VISIT which, const int depth){
	node = *(void**)node;
	addr_sock_lock* sock = get_socket(*(struct in_addr*)node);
	pthread_mutex_lock(&(sock->lock));
	
	int data[2];
	data[0] = UPDATED;
	data[1] = ((addr_page*)node) -> pageno;
	send(sock -> socketfd, data, 8, 0);

	pthread_mutex_unlock(&(sock->lock));
}

void notify_write(int page){
	twalk(readers_list[page], notify_single);
}

void add_reader(int page, struct in_addr addr){
	addr_page* key = malloc(sizeof(addr_page));
	key -> addr = addr;
	addr_page* res = tsearch(key, &readers_list[page], cmp_node);
	res = *(void**) res;
	if(res != key){
		free(key);
	}
	else{
		key -> pageno = page;
	}
}

void* free_thing(void* thing){free(thing);return NULL;}

void* handle_connection(void* sock_ptr){
	int socket = *(int*)sock_ptr;
	free(sock_ptr);
	struct sockaddr_in addr_t;
	int sz = sizeof(addr_t);
	getpeername(socket, (struct sockaddr*)&addr_t, &sz);
       	struct in_addr addr = addr_t.sin_addr;

	int data[2];
	while(1){
		recv(socket, data, 8, 0);
		int req_type = data[0];
		data[0] = data[1];
		pthread_mutex_lock(page_locks + data[1]);
		if(req_type == UPDATED && !own[data[1]]){
			pages[data[1]] = -1;	
			pthread_mutex_lock(&print_lock);
			printf("Getting update notice for %d\n", data[1]);
			pthread_mutex_unlock(&print_lock);
		}
		else{
			data[1] = pages[data[1]];
			/* write request, delete page and notify readers to delete */
			if(req_type == REQ_WRITE){
				pthread_mutex_lock(&print_lock);
				printf("Getting write request for %d from %s\n", data[0], inet_ntoa(addr));
				pthread_mutex_unlock(&print_lock);

				notify_write(data[0]);
				pages[data[0]] = -1;
				own[data[0]] = 0;
				tdestroy(readers_list[data[0]], free_thing);
				readers_list[data[0]] = NULL;
			}
			/* read request, add reader to list */
			else{
				pthread_mutex_lock(&print_lock);
				printf("Getting read request for %d from %s\n", data[0], inet_ntoa(addr));
				pthread_mutex_unlock(&print_lock);

				add_reader(data[0], addr); 	
			}
			send(socket, data, 8, 0);
			pthread_mutex_lock(&print_lock);
			printf("Sent value for %d: %d to %s\n", data[0], data[1], inet_ntoa(addr));
			pthread_mutex_unlock(&print_lock);
		}
		pthread_mutex_unlock(page_locks + data[0]);
	}
}

void* do_read(void* a){
	int data[2];
	pthread_mutex_lock(&rand_lock);
	int page = available_pages[rand() % available_pages_n];
	pthread_mutex_unlock(&rand_lock);
	pthread_mutex_lock(page_locks + page);
	if(pages[page] != -1){
		pthread_mutex_lock(&print_lock);
		printf("Reading %d from memory: %d\n", page, pages[page]);
		pthread_mutex_unlock(&print_lock);
	}
	else{
		data[0] = REQ_READ;
		data[1] = page;
		pthread_mutex_lock(&print_lock);
		printf("Sending read request for %d to server\n", page);
		pthread_mutex_unlock(&print_lock);

		pthread_mutex_lock(&server_lock);
		send(server_socket, data, 8, 0);
		recv(server_socket, data, 8, 0);
		if(data[1] == 0){
			pthread_mutex_lock(&print_lock);
			printf("Creating new page for reading: %d\n", page);
			pthread_mutex_unlock(&print_lock);

			pages[page] = 1;
			own[page] = 1;
		}	
		else{
			addr_sock_lock* sock = get_socket(*(struct in_addr*)&data[1]);
			pthread_mutex_lock(&print_lock);
			printf("Sending read request for %d to %s\n", page, inet_ntoa(sock -> addr));
			pthread_mutex_unlock(&print_lock);

			pthread_mutex_lock(&(sock->lock));
			data[0] = REQ_READ;
			data[1] = page;
			send(sock -> socketfd, data, 8, 0);
			recv(sock -> socketfd, data, 8, 0);
			pthread_mutex_unlock(&(sock->lock));

			pthread_mutex_lock(&print_lock);
			printf("Got value %d for page %d\n", data[1], page);
			pthread_mutex_unlock(&print_lock);

			pages[page] = data[1];
		}
		data[0] = CONFIRM;
		data[1] = page;
		send(server_socket, data, 8, 0);
		pthread_mutex_lock(&print_lock);
		printf("Sent confirmation to server for %d\n", page);
		pthread_mutex_unlock(&print_lock);

		pthread_mutex_unlock(&server_lock);
	}
	
	pthread_mutex_unlock(page_locks + page);
	return NULL;
}

void* do_write(void* a){
	int data[2];
	pthread_mutex_lock(&rand_lock);
	int page = available_pages[rand() % available_pages_n];
	pthread_mutex_unlock(&rand_lock);
	pthread_mutex_lock(page_locks + page);
	if(own[page]){
		notify_write(page);
		pages[page]++;	

		pthread_mutex_lock(&print_lock);
		printf("Writing to %d in memory, new value: %d\n", page, pages[page]);
		pthread_mutex_unlock(&print_lock);
	}
	else{
		data[0] = REQ_WRITE;
		data[1] = page;
		pthread_mutex_lock(&print_lock);
		printf("Sending write request for %d to server\n", page);
		pthread_mutex_unlock(&print_lock);

		pthread_mutex_lock(&server_lock);
		send(server_socket, data, 8, 0);
		recv(server_socket, data, 8, 0);
		if(data[1] == 0){
			pthread_mutex_lock(&print_lock);
			printf("Creating new page for writing: %d\n", page);
			pthread_mutex_unlock(&print_lock);

			pages[page] = 1;
			own[page] = 1;
		}	
		else{	
			addr_sock_lock* sock = get_socket(*(struct in_addr*)&data[1]);
			pthread_mutex_lock(&print_lock);
			printf("Sending write request for %d to %s\n", page, inet_ntoa(sock -> addr));
			pthread_mutex_unlock(&print_lock);

			pthread_mutex_lock(&(sock->lock));
			data[0] = REQ_WRITE;
			data[1] = page;
			send(sock -> socketfd, data, 8, 0);
			recv(sock -> socketfd, data, 8, 0);
			pthread_mutex_unlock(&(sock->lock));
			pthread_mutex_lock(&print_lock);
			printf("Got value %d for page %d, writing %d\n", data[1], page, data[1]+1);
			pthread_mutex_unlock(&print_lock);

			pages[page] = data[1];
			pages[page]++;
			own[page] = 1;
		}
		data[0] = CONFIRM;
		data[1] = page;
		send(server_socket, data, 8, 0);
		pthread_mutex_lock(&print_lock);
		printf("Sent confirmation to server for %d\n", page);
		pthread_mutex_unlock(&print_lock);

		pthread_mutex_unlock(&server_lock);
	}
	pthread_mutex_unlock(page_locks + page);
	return NULL;
}

void* event_generator(void* a){
	srand(time(NULL));
	pthread_mutex_init(&rand_lock, NULL);
	pthread_mutex_lock(&rand_lock);
	while(1){
		/* generate random wait time and wait it */
		int sleept = rand();
		pthread_mutex_unlock(&rand_lock);
		if(!sleept) sleept = 1;
		sleept = log((double)sleept/(double)RAND_MAX) * -(double)exp_mean;
		sleept *= 1000;
		usleep(sleept);

		void*(*req_type)(void*);
		pthread_mutex_lock(&rand_lock);
		if(rand()%100 < read_prob){
			req_type = do_read;	
		}
		else req_type = do_write;
		pthread_t trash;
		pthread_create(&trash, &threads_attr, req_type, NULL);
			
	}
}

int main(void){
	char addr_in[20];

	scanf("%s %hu %u %u %u", addr_in, &port, &exp_mean, &read_prob, &page_n);
	pages = malloc(sizeof(int) * page_n);
	memset(pages, -1, 4*page_n);
	own = calloc(page_n, sizeof(int));
	available_pages = malloc(sizeof(int) * page_n);
	page_locks = malloc(sizeof(pthread_mutex_t) * page_n);
	int i;
	for(i = 0; i < page_n; i++) pthread_mutex_init(page_locks+i, NULL);
	readers_list = calloc(sizeof(void*), page_n);

	int page;
	while(scanf("%d", &page) != EOF){
		available_pages[available_pages_n] = page;
		available_pages_n++;
	} 

	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	port = htons(port);
	server_addr.sin_port = port;
	inet_pton(AF_INET, addr_in, &(server_addr.sin_addr));
	memset(&(server_addr.sin_zero), 0, 8);
	server_socket = socket(PF_INET, SOCK_STREAM, 0);
	connect(server_socket, (struct sockaddr*)&server_addr, sizeof(struct sockaddr_in));

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = port;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	memset(&(server_addr.sin_zero), 0, 8);

	int listening_socket = socket(PF_INET, SOCK_STREAM, 0);
	bind(listening_socket, (struct sockaddr*)&server_addr, sizeof server_addr);
	listen(listening_socket, 20);

	pthread_attr_init(&threads_attr);
	pthread_attr_setdetachstate(&threads_attr, PTHREAD_CREATE_DETACHED);
	pthread_mutex_init(&tree_lock, NULL);
	pthread_mutex_init(&server_lock, NULL);
	pthread_mutex_init(&print_lock, NULL);
	pthread_t trash;
	pthread_create(&trash, &threads_attr, event_generator, NULL);
	while(1){
		int* new_socket = malloc(4);
		*new_socket = accept(listening_socket, NULL, NULL); 
		if(new_socket < 0) printf("asd\n");
		pthread_create(&trash, &threads_attr, handle_connection, new_socket);
	}

	close(listening_socket);

	return 0;
}

