/* client -> server: [REQ_X, page_no]
 * server -> client: [page_no, ownder_addr]
 * client -> owner:  [REQ_X, page_no]
 * owner -> clients: [UPDATED, page_no]
 * owner  -> client: [page_no, page]
 * client -> server: [CONFIRM, page_no]
 */

#define REQ_WRITE (1<<27)   
#define REQ_READ  (1<<28)
#define CONFIRM   (1<<29)
#define UPDATED   (1<<30)
#define RESPONSE  (1<<31)
