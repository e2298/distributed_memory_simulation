CC = gcc -std=gnu90
COPTIONS = -pthread

all: run clean

server: server.c
	$(CC) server.c $(COPTIONS) -o server

client: client.c
	$(CC) client.c $(COPTIONS) -lm -o client

.PHONY: clean run_server run_client

run_server: server
	./server < server_input

run_client: client
	./client < client_input

clean: 
	rm server client

