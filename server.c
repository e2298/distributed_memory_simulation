#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<pthread.h>
#include"defs.h"

int listening_socket;
unsigned page_n; /*number of pages */
pthread_mutex_t* pages_m;
struct in_addr* pages_addr;
int *changing_owner;
pthread_mutex_t print_lock;

void* handle_client(void* sock_ptr){
	int socket = *(int*)sock_ptr;
	free(sock_ptr);
	struct sockaddr_in addr_t;
	int sz = sizeof(addr_t);
	getpeername(socket, (struct sockaddr*)&addr_t, &sz);
       	struct in_addr addr = addr_t.sin_addr;
	pthread_mutex_lock(&print_lock);
	printf("Got new connection to %s\n", inet_ntoa(addr));
	pthread_mutex_unlock(&print_lock);

	int data[2];
	while(1){
		recv(socket, data, 8, 0);
		int page = data[1];

		if(data[0] == CONFIRM){
			pthread_mutex_lock(&print_lock);
			printf("Got confirm for %d from %s\n", page, inet_ntoa(addr));
			pthread_mutex_unlock(&print_lock);
		
			if(changing_owner[data[1]]) {
				pages_addr[data[1]] = addr;
			       	changing_owner[data[1]] = 0;
				pthread_mutex_lock(&print_lock);
				printf("changing owner of %d to %s\n", page, inet_ntoa(addr));
				pthread_mutex_unlock(&print_lock);
			
			};
			pthread_mutex_lock(&print_lock);
			printf("Release lock %d\n", page);
			pthread_mutex_unlock(&print_lock);
		
			pthread_mutex_unlock(&pages_m[data[1]]);		
		}
		else{
			pthread_mutex_lock(&print_lock);
			if(data[0] == REQ_WRITE)
				printf("Got write request for %d from %s\n", page, inet_ntoa(addr));
			else
				printf("Got read request for %d from %s\n", page, inet_ntoa(addr));
			pthread_mutex_unlock(&print_lock);

			pthread_mutex_lock(&print_lock);
			printf("Try lock %d\n", page);
			pthread_mutex_unlock(&print_lock);
		
			pthread_mutex_lock(&pages_m[data[1]]);
			pthread_mutex_lock(&print_lock);
			printf("Got lock %d\n", page);
			pthread_mutex_unlock(&print_lock);
		
			if(data[0] == REQ_WRITE || *(int*)&pages_addr[data[1]] == 0) changing_owner[data[1]] = 1;
			data[0] = data[1];
			data[1] = *(int*)&pages_addr[data[1]];
			send(socket, data, 8, 0);	
		}		
	}
	return NULL;
}

int main(void){
	uint16_t port; 
	struct sockaddr_in server_addr;
	scanf("%hd %u", &port, &page_n);
	pages_addr = calloc(page_n, 4);
	changing_owner = calloc(page_n, 4);
	pages_m = calloc(page_n, sizeof(pthread_mutex_t));
	int i;
	for(i = 0; i < page_n; i++) pthread_mutex_init(&pages_m[i], NULL);
	
	port = htons(port);
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = port;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	memset(&(server_addr.sin_zero), 0, 8);

	listening_socket = socket(PF_INET, SOCK_STREAM, 0);
	bind(listening_socket, (struct sockaddr*)&server_addr, sizeof server_addr);
	listen(listening_socket, 20);

	pthread_attr_t threads_attr;
	pthread_attr_init(&threads_attr);
	pthread_attr_setdetachstate(&threads_attr, PTHREAD_CREATE_DETACHED);
	pthread_mutex_init(&print_lock, NULL);
	while(1){
		int* new_socket = malloc(4);
		*new_socket = accept(listening_socket, NULL, NULL); 
		if(new_socket < 0) printf("asd\n");
		pthread_t trash;
		pthread_create(&trash, &threads_attr, handle_client, new_socket);
	}

	close(listening_socket);

	return 0;
}
